<?php

declare(strict_types=1);

namespace App\Event;

use App\Entity\Rds;
use Symfony\Component\EventDispatcher\Event;

class RdsEvent extends Event
{
    /** @var Rds */
    private $rds;

    public function __construct(Rds $rds)
    {
        $this->rds = $rds;
    }

    public function getRds(): Rds
    {
        return $this->rds;
    }
}