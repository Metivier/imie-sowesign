<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180704200102 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE document (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', path_name VARCHAR(255) NOT NULL, uploaded_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attendee (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', rds_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, organization VARCHAR(255) DEFAULT NULL, token VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1150D5675F37A13B (token), INDEX IDX_1150D5677B10CFB4 (rds_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE signature_slot (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', attende_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', page INT NOT NULL, position_x DOUBLE PRECISION NOT NULL, position_y DOUBLE PRECISION NOT NULL, signed_at DATETIME DEFAULT NULL, INDEX IDX_F3A69DBFC33F7837 (document_id), INDEX IDX_F3A69DBF4AC0CD7D (attende_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rds (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', owner_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', description LONGTEXT NOT NULL, start_at DATETIME NOT NULL, end_at DATETIME NOT NULL, place VARCHAR(255) DEFAULT NULL, token VARCHAR(255) NOT NULL, revival_number INT DEFAULT NULL, periodicity INT DEFAULT NULL, UNIQUE INDEX UNIQ_60A9EB095F37A13B (token), UNIQUE INDEX UNIQ_60A9EB09C33F7837 (document_id), UNIQUE INDEX UNIQ_60A9EB097E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attendee ADD CONSTRAINT FK_1150D5677B10CFB4 FOREIGN KEY (rds_id) REFERENCES rds (id)');
        $this->addSql('ALTER TABLE signature_slot ADD CONSTRAINT FK_F3A69DBFC33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE signature_slot ADD CONSTRAINT FK_F3A69DBF4AC0CD7D FOREIGN KEY (attende_id) REFERENCES attendee (id)');
        $this->addSql('ALTER TABLE rds ADD CONSTRAINT FK_60A9EB09C33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE rds ADD CONSTRAINT FK_60A9EB097E3C61F9 FOREIGN KEY (owner_id) REFERENCES attendee (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signature_slot DROP FOREIGN KEY FK_F3A69DBFC33F7837');
        $this->addSql('ALTER TABLE rds DROP FOREIGN KEY FK_60A9EB09C33F7837');
        $this->addSql('ALTER TABLE signature_slot DROP FOREIGN KEY FK_F3A69DBF4AC0CD7D');
        $this->addSql('ALTER TABLE rds DROP FOREIGN KEY FK_60A9EB097E3C61F9');
        $this->addSql('ALTER TABLE attendee DROP FOREIGN KEY FK_1150D5677B10CFB4');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE attendee');
        $this->addSql('DROP TABLE signature_slot');
        $this->addSql('DROP TABLE rds');
    }
}
