<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180705062618 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attendee DROP FOREIGN KEY FK_1150D5677B10CFB4');
        $this->addSql('ALTER TABLE attendee CHANGE rds_id rds_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE organization organization VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE attendee ADD CONSTRAINT FK_1150D5677B10CFB4 FOREIGN KEY (rds_id) REFERENCES rds (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE signature_slot CHANGE document_id document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE attende_id attende_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE signed_at signed_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE rds CHANGE document_id document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE owner_id owner_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE place place VARCHAR(255) DEFAULT NULL, CHANGE revival_number revival_number INT DEFAULT NULL, CHANGE periodicity periodicity INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attendee DROP FOREIGN KEY FK_1150D5677B10CFB4');
        $this->addSql('ALTER TABLE attendee CHANGE rds_id rds_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE organization organization VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE attendee ADD CONSTRAINT FK_1150D5677B10CFB4 FOREIGN KEY (rds_id) REFERENCES rds (id)');
        $this->addSql('ALTER TABLE rds CHANGE document_id document_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE owner_id owner_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE place place VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE revival_number revival_number INT DEFAULT NULL, CHANGE periodicity periodicity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE signature_slot CHANGE document_id document_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE attende_id attende_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE signed_at signed_at DATETIME DEFAULT \'NULL\'');
    }
}
