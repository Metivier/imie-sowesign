<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180706095128 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attendee CHANGE rds_id rds_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE organization organization VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE signature_slot CHANGE document_id document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE attende_id attende_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE signed_at signed_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE rds CHANGE document_id document_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE owner_id owner_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE place place VARCHAR(255) DEFAULT NULL, CHANGE revival_number revival_number INT DEFAULT NULL, CHANGE periodicity periodicity INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attendee CHANGE rds_id rds_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE email email VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE organization organization VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE rds CHANGE document_id document_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE owner_id owner_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE place place VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE revival_number revival_number INT DEFAULT NULL, CHANGE periodicity periodicity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE signature_slot CHANGE document_id document_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE attende_id attende_id CHAR(36) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', CHANGE signed_at signed_at DATETIME DEFAULT \'NULL\'');
    }
}
