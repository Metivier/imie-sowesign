<?php
declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class SignatureSlotExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('signatureSlot', array($this, 'signatureSlot')),
        );
    }

    public function signatureSlot($slot)
    {
        return str_pad((string) $slot, 3, "0", STR_PAD_LEFT);
    }
}