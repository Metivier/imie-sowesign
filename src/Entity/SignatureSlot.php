<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class SignatureSlot
{

    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $page;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $positionX;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $positionY;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $signedAt;

    /**
     * @var Document
     * @ORM\ManyToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $document;

    /**
     * @var Attendee
     * @ORM\ManyToOne(targetEntity="App\Entity\Attendee", inversedBy="slots", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="attendee_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $attende;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return float
     */
    public function getPositionX(): float
    {
        return $this->positionX;
    }

    /**
     * @param float $positionX
     */
    public function setPositionX(float $positionX): void
    {
        $this->positionX = $positionX;
    }

    /**
     * @return float
     */
    public function getPositionY(): float
    {
        return $this->positionY;
    }

    /**
     * @param float $positionY
     */
    public function setPositionY(float $positionY): void
    {
        $this->positionY = $positionY;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getSignedAt(): ?\DateTimeInterface
    {
        return $this->signedAt;
    }

    public function setSigned(): void
    {
        $this->signedAt = new \DateTimeImmutable();
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

    /**
     * @param Document $document
     */
    public function setDocument(Document $document): void
    {
        $this->document = $document;
    }

    /**
     * @return Attendee
     */
    public function getAttende(): Attendee
    {
        return $this->attende;
    }

    /**
     * @param Attendee $attende
     */
    public function setAttende(Attendee $attende): void
    {
        $this->attende = $attende;
    }
}