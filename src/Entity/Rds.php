<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Attendee;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RdsRepository")
 */
class Rds
{
    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $startAt;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $place;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $token;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $revivalNumber;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $periodicity;

    /**
     * @var Document
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     */
    private $document;

    /**
     * @var ArrayCollection|Attendee[]
     * @ORM\OneToMany(targetEntity="App\Entity\Attendee", mappedBy="rds", cascade={"persist", "remove"})
     */
    private $attendees;

    /**
     * @var Attendee
     * @ORM\OneToOne(targetEntity="App\Entity\Attendee", cascade={"persist", "remove"})
     */
    private $owner;

    public function __construct()
    {
        $this->attendees = new ArrayCollection();
        $this->startAt = new \DateTimeImmutable();
        $this->endAt = new \DateTimeImmutable('+3 hours');
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject ?? '';
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getStartAt(): \DateTimeInterface
    {
        return $this->startAt;
    }

    /**
     * @param \DateTimeInterface $startAt
     */
    public function setStartAt(\DateTimeInterface $startAt): void
    {
        $this->startAt = $startAt;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEndAt(): \DateTimeInterface
    {
        return $this->endAt;
    }

    /**
     * @param \DateTimeInterface $endAt
     */
    public function setEndAt(\DateTimeInterface $endAt): void
    {
        $this->endAt = $endAt;
    }

    /**
     * @return string
     */
    public function getPlace(): ?string
    {
        return $this->place;
    }

    /**
     * @param string $place
     */
    public function setPlace(string $place): void
    {
        $this->place = $place;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getRevivalNumber(): ?int
    {
        return $this->revivalNumber;
    }

    /**
     * @param int $revivalNumber
     */
    public function setRevivalNumber(int $revivalNumber): void
    {
        $this->revivalNumber = $revivalNumber;
    }

    /**
     * @return int
     */
    public function getPeriodicity(): ?int
    {
        return $this->periodicity;
    }

    /**
     * @param int $periodicity
     */
    public function setPeriodicity(int $periodicity): void
    {
        $this->periodicity = $periodicity;
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

    /**
     * @param Document $document
     */
    public function setDocument(Document $document): void
    {
        $this->document = $document;
    }

    /**
     * @return Attendee
     */
    public function getOwner(): Attendee
    {
        return $this->owner;
    }

    /**
     * @param Attendee $owner
     */
    public function setOwner(Attendee $owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return Attendee[]|ArrayCollection
     */
    public function getAttendees()
    {
        return $this->attendees;
    }

    /**
     * @param Attendee[]|ArrayCollection $attendees
     */
    public function setAttendees($attendees): void
    {
        $this->attendees = $attendees;
    }

    public function addAttendee(Attendee $attendee): void
    {
        $this->attendees->add($attendee);
    }
}