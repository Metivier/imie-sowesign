<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class Document
{
    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var File
     * @Vich\UploadableField(mapping="document", fileNameProperty="pathName")
     * @Assert\File(mimeTypes={"application/pdf"})
     */
    private $documentFile;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $pathName;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $uploadedAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $originalCopyPath;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $originalHash;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPathName(): ?string
    {
        return $this->pathName;
    }

    /**
     * @param string $pathName
     */
    public function setPathName(?string $pathName = null): void
    {
        $this->pathName = $pathName;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUploadedAt(): \DateTimeInterface
    {
        return $this->uploadedAt;
    }

    /**
     * @param \DateTimeInterface $uploadedAt
     */
    public function setUploadedAt(\DateTimeInterface $uploadedAt): void
    {
        $this->uploadedAt = $uploadedAt;
    }

    public function setDocumentFile(?File $image = null): void
    {
        $this->documentFile = $image;

        if (null !== $image) {
            $this->uploadedAt = new \DateTimeImmutable();
        }
    }

    public function getDocumentFile(): ?File
    {
        return $this->documentFile;
    }

    public function getOriginalCopyPath(): ?string
    {
        return $this->originalCopyPath;
    }

    public function setOriginalCopyPath(?string $originalCopyPath = null): void
    {
        $this->originalCopyPath = $originalCopyPath;

        $this->originalHash = md5_file($originalCopyPath);
    }

    public function getOriginalHash(): ?string
    {
        return $this->originalHash;
    }
}