<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Attendee
{
    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstname;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastname;

    /**
     * @var null|string
     * @Assert\Email()
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $organization;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $token;

    /**
     * @var \App\Entity\Rds
     * @ORM\ManyToOne(targetEntity="App\Entity\Rds", inversedBy="attendees")
     * @ORM\JoinColumn(name="rds_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $rds;

    /**
     * @var SignatureSlot
     * @ORM\OneToMany(targetEntity="App\Entity\SignatureSlot", mappedBy="attende", cascade={"persist", "remove"})
     */
    private $slots;

    /**
     * @var null|int
     * @ORM\Column(type="integer", length=3, nullable=true)
     */
    private $tag;

    public function __construct()
    {
        $this->slots = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname ?? '';
    }

    /**
     * @param null|string $firstname
     */
    public function setFirstname(?string $firstname = null): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return null|string
     */
    public function getLastname(): ?string
    {
        return $this->lastname ?? '';
    }

    /**
     * @param null|string $lastname
     */
    public function setLastname(?string $lastname = null): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email ?? '';
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email = null): void
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getOrganization(): ?string
    {
        return $this->organization;
    }

    /**
     * @param null|string $organization
     */
    public function setOrganization(?string $organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return Rds
     */
    public function getRds(): Rds
    {
        return $this->rds;
    }

    /**
     * @param Rds $rds
     */
    public function setRds(Rds $rds): void
    {
        $this->rds = $rds;
    }

    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * @param SignatureSlot $slots
     */
    public function setSlots(SignatureSlot $slots): void
    {
        $this->slots = $slots;
    }

    /**
     * @return int|null
     */
    public function getTag(): ?int
    {
        return $this->tag;
    }

    /**
     * @param int|null $tag
     */
    public function setTag(?int $tag): void
    {
        $this->tag = $tag;
    }
}