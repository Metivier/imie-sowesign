<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Document;
use App\Entity\Rds;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class GarbageCreationHandler
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(Session $session)
    {
        $documentId = $session->get('document');
        if (!$documentId) {
            return;
        }

        $document = $this->entityManager->getRepository(Document::class)->find($documentId);
        if (!$document instanceof Document) {
            return;
        }

        $jsonFile =  $document->getDocumentFile()->getPathname().'.json';

        if (file_exists($jsonFile)) {
            unlink($jsonFile);
        }

        if (file_exists($document->getOriginalCopyPath())) {
            unlink($document->getOriginalCopyPath());
        }

        $rds = $this->entityManager->getRepository(Rds::class)->findOneBy(['document' => $documentId]);
        if ($rds instanceof Rds) {
            $this->entityManager->remove($rds);
        }

        $this->entityManager->remove($document);
        $this->entityManager->flush();

        return;
    }
}