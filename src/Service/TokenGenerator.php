<?php

declare(strict_types=1);

namespace App\Service;

class TokenGenerator
{
    public function generate(): string
    {
        return sha1(microtime());
    }
}