<?php

namespace App\Service\Email;

use App\Entity\Attendee;
use App\Entity\Rds;

class RdsEmailGenerator
{
    private $templating;

    public function __construct(\Twig_Environment $templating)
    {
        $this->templating = $templating;
    }

    public function generate(Rds $rds, Attendee $attendee): \Swift_Message
    {

        $message = (new \Swift_Message($rds->getSubject()))
            ->setFrom('no-reply@groupe1.sowesign.fr')
            ->setTo($attendee->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/rds_invitation.html.twig', [
                        'rds' => $rds,
                        'attendee' => $attendee
                    ]
                ),
                'text/html'
            )
        ;

        return $message;
    }
}