<?php

declare(strict_types=1);

namespace App\Service\Pdf;

use App\Model\Attendee;
use App\Model\PdfTags;
use App\Service\PdfParser;
use Smalot\PdfParser\Parser;
use Symfony\Component\HttpFoundation\File\File;

class TagsCollector
{
    private $pdfParser;

    public function __construct()
    {
        $this->pdfParser = new Parser();
    }

    public function TagFromPdf(File $file): PdfTags
    {
        $pdfTags = new PdfTags();

        $pdf = $this->pdfParser->parseFile($file->getPathname());
        $text = $pdf->getText();

        $this->parseContent($text, $pdfTags);

        return $pdfTags;
    }

    private function parseContent(string $str, PdfTags $pdfTags)
    {
        $strClean = str_replace(["\t"],"", $str);
        $raws = explode("\n", $strClean);

        foreach ($raws as $raw) {
            if ($admin = $this->findAdminTags($raw)) {
                $pdfTags->setAdmin($admin);
            };
            $pdfTags = $this->findRdsTags($raw, $pdfTags);
            $pdfTags = $this->findScheduleTags($raw, $pdfTags);
            if (!$pdfTags->hasSignTags()) {
                $pdfTags->setHasSignTags($this->findSignTags($raw));
            }
        }
    }

    private function findAdminTags(string $str): ?Attendee
    {
        if (false !== ($position = strpos($str, '#ADMIN#'))) {
            $substr = substr($str, $position+1);
            $items = explode('#', $substr);

            $admin = new Attendee();
            $admin->setFirstName($items[1] ?? null);
            $admin->setLastName($items[2] ?? null);
            $admin->setEmail($items[3] ?? null);
            $admin->setOrganizaiton($items[4] ?? null);

            return $admin;
        };

        return null;
    }

    private function findRdsTags(string $str, PdfTags $pdfTags): PdfTags
    {
        if (false !== $position = strpos($str, '#MEETING#')) {
            $substr = substr($str, $position + 1);
            $items = explode('#', $substr);

            $pdfTags->setRdsSubject($items[1] ?? null);
            $pdfTags->setRdsDescription($items[2] ?? null);
        }

        return $pdfTags;
    }

    private function findScheduleTags(string $str, PdfTags $pdfTags): PdfTags
    {
        if (false !== $position = strpos($str, '#SCHEDULE#')) {
            $substr = substr($str, $position + 1);
            $items = explode('#', $substr);
            if (preg_match('/^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/', $items[1])) {
                $date = new \DateTimeImmutable($items[3]);
                $pdfTags->setRdsStartAt($date);
            }

            if (isset($items[2]) && preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/', $items[2]) && $pdfTags instanceof \DateTimeInterface) {
                $hour = $substr($items[2], 0, 2);
                $minute = $substr($items[2], 3, 5);
                $pdfTags->setRdsStartAt($pdfTags->getRdsStartAt()->setTime($hour, $minute));
            }

            $pdfTags->setRdsPlace($items[3] ?? null);
        }

        return $pdfTags;
    }

    private function findSignTags(string $str): bool
    {
        return 1 === preg_match('/#SIGN(\d{3})#/', $str);
    }
}