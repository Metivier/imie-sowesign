<?php

declare(strict_types=1);

namespace App\Service\Pdf;

use App\Entity\Rds;
use App\Entity\SignatureSlot;
use App\Entity\Attendee;
use App\Service\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;

class SignedSlotCollector
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var TokenGenerator */
    private $tokenGenerator;

    /**
     * SignedSlotCollector constructor.
     * @param EntityManagerInterface $em
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(EntityManagerInterface $em, TokenGenerator $tokenGenerator)
    {
        $this->em = $em;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param Rds $rds
     */
    public function collector(Rds $rds)
    {
        $fileContent = file_get_contents($rds->getDocument()->getDocumentFile().'.json');
        $content = json_decode(utf8_encode($fileContent), true);
        $signatureSlots = [];
        foreach ($content as $page)
        {
            foreach ($page['text'] as $text)
            {
                if (preg_match('/#SIGN(\d{3})#/', $text['data'], $matches)) {
                    $text['page'] = $page['number'];
                    $text['pageH'] = $page['height'];
                    $text['pageW'] = $page['width'];
                    $signatureSlots[$matches[1]][] = $text;
                }
            }
        }

        $this->generateSignatureSlot($signatureSlots, $rds);
    }

    private function generateSignatureSlot(array $signatureSlots, Rds $rds)
    {
        foreach ($signatureSlots as $key => $slots) {
            $attendee = new Attendee();
            $attendee->setToken($this->tokenGenerator->generate());
            $attendee->setTag((int) $key);
            $attendee->setRds($rds);

            foreach ($slots as $slot) {
                $signatureSlot = new SignatureSlot();
                $signatureSlot->setDocument($rds->getDocument());
                $signatureSlot->setAttende($attendee);
                $signatureSlot->setPage($slot['page']);
                $signatureSlot->setPositionX($slot['left']*100/$slot['pageW']);
                $signatureSlot->setPositionY($slot['top']*100/$slot['pageH']);

                $this->em->persist($signatureSlot);
            }

            $this->em->persist($attendee);
        }

        $this->em->flush();
    }
}