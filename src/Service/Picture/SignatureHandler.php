<?php

declare(strict_types=1);

namespace App\Service\Picture;

use App\Entity\Attendee;
use App\Entity\SignatureSlot;
use App\Service\PictureBase64;
use App\Service\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use setasign\Fpdi\Fpdi;

class SignatureHandler
{
    /** @var TokenGenerator */
    private $tokenGenerator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(TokenGenerator $tokenGenerator, EntityManagerInterface $entityManager)
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->entityManager = $entityManager;
    }

    public function handle(Attendee $attendee, string $signatureBase64, string $filesPath)
    {
        $fileName = $this->tokenGenerator->generate();

        $signature = PictureBase64::base64_to_jpeg($signatureBase64, $filesPath, $fileName);

        foreach ($attendee->getSlots() as $slot)
        {
            /** @var SignatureSlot $slot */
            $this->signatureInPdf($signature, $slot);

            $slot->setSigned();
        }

        $this->entityManager->flush();

    }

    private function signatureInPdf($signature, SignatureSlot $slot)
    {

        $pdf = new Fpdi('P');
        $pdfNbPage = $pdf->setSourceFile($slot->getDocument()->getDocumentFile()->getPathname());

// Set the default font to use
        $pdf->SetFont('arial');

        // Add a page
        for ($i = 1; $i <= $pdfNbPage; $i++) {
            $pdf->AddPage();
            $tpl = $pdf->importPage($i);
            $pdf->useTemplate($tpl);

            if ($i === $slot->getPage()) {
                $tplSize = $pdf->getTemplateSize($tpl);
                $pdf->SetXY(($tplSize['width']*$slot->getPositionX()/100), ($tplSize['height']*$slot->getPositionY()/100));
                $pdf->Cell(0, 0, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 40), 0, 0, 'L');
            }
        }

        $pdfFile = $slot->getDocument()->getDocumentFile();
        unlink($signature);
        return $pdf->Output('F', $pdfFile->getPath().'/'.$pdfFile->getFilename());
    }

}