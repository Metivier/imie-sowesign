<?php

declare(strict_types=1);

namespace App\Service;

class PictureBase64
{
    static function base64_to_jpeg(string $base64String, string $filesPath, string $fileName) :string
    {
        $ifp = fopen( $filesPath.$fileName.'.png', 'wb' );
        $data = explode( ',', $base64String );
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        fclose( $ifp );

        return self::pngTojpg($filesPath.$fileName.'.png', $filesPath, $fileName);
    }

    static function pngTojpg($pngPath, string $filesPath, $fileName)
    {
        $image = imagecreatefrompng($pngPath);
        $bg = imagecreatetruecolor(800, 800);
        imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
        imagealphablending($bg, TRUE);
        imagecopyresized($bg, $image, 0, 0, 0, 0, 800, 800, imagesx($image), imagesy($image));
        imagedestroy($image);
        $quality = 100; // 0 = worst / smaller file, 100 = better / bigger file
        imagejpeg($bg, $filesPath.$fileName. ".jpg", $quality);
        imagedestroy($bg);
        unlink($pngPath);

        return $filesPath.$fileName. ".jpg";
    }
}