<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Rds;
use App\Model\SignatureProgress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class RdsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rds::class);
    }

    public function findRdsByToken(string $token)
    {
        return $this->createQueryBuilder('rds')
            ->leftJoin('rds.attendees', 'attendee')
            ->leftJoin('rds.owner', 'owner')
            ->addSelect(['attendee', 'owner'])
            ->where('attendee.token = :token')
            ->orWhere('owner.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult();
            ;
    }

    public function signatureProgress(Rds $rds)
    {
        $queryStr = 'SELECT NEW '.SignatureProgress::class.'(
                    (
                        SELECT (COUNT(DISTINCT(attendees_2.id))) FROM '.Rds::class.' rds_2
                        JOIN rds_2.attendees attendees_2
                        JOIN attendees_2.slots slot
                        WHERE rds_2.id = :rds_id
                        AND slot.signedAt is not null
                    ), 
                    COUNT(attendee.id)
                    ) 
                FROM '.Rds::class.' rds
                LEFT JOIN rds.attendees attendee
                WHERE rds.id = :rds_id
                ';
        $query = $this->getEntityManager()->createQuery($queryStr);
        $query->setParameter('rds_id', $rds->getId());

        return $query->getSingleResult();
    }
}