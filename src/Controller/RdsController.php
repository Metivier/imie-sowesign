<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Attendee;
use App\Entity\Rds;
use App\Service\Picture\SignatureHandler;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RdsController
 * @Route("/rds")
 */
class RdsController extends Controller
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{token}", name="rds-show")
     * @Method({"GET"})
     */
    public function indexAction($token): Response
    {
        $rds = $this->entityManager->getRepository(Rds::class)->findRdsByToken($token);

        if (!$rds) {
            throw $this->createNotFoundException();
        }

        $signatureProgress = $this->entityManager->getRepository(Rds::class)->signatureProgress($rds);
        $user = $this->entityManager->getRepository(Attendee::class)->findOneBy(['token' => $token]);

        return $this->render('rds/show.html.twig', [
            'rds' => $rds,
            'user' => $user,
            'progress' => $signatureProgress,
            'is_admin' => $rds->getOwner()->getToken() === $token
        ]);
    }

    /**
     * @Route("/{token}", name="rds-sign")
     * @Method({"POST"})
     */
    public function signAction(Request $request, $token, SignatureHandler $signatureHandler): Response
    {
        /** @var Rds $rds */
        $rds = $this->entityManager->getRepository(Rds::class)->findRdsByToken($token);

        if (!$rds) {
            throw $this->createNotFoundException();
        }

        $user = $this->entityManager->getRepository(Attendee::class)->findOneBy(['token' => $token]);

        $content = json_decode($request->getContent());
        $signature = $content->imageData;
        $filesPath = $rds->getDocument()->getDocumentFile()->getPath().'/';

        $signatureHandler->handle($user, $signature, $filesPath);

        return new JsonResponse('ok');
    }
}
