<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Attendee;
use App\Entity\Document;
use App\Entity\Rds;
use App\Event\DelayedEventDispatcher;
use App\Event\RdsEvent;
use App\Form\AdminType;
use App\Form\DocumentType;
use App\Form\RdsAttendeeType;
use App\Form\RdsType;
use App\Service\Pdf\TagsCollector;
use App\Service\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RdsCreationController extends Controller
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="homepage")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request): Response
    {
        $document = new Document();

        $form = $this->createForm(DocumentType::class, $document, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('homepage')
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($document);
            $this->entityManager->flush();

            $session = $request->getSession();
            $session->set('document', $document->getId());

            return $this->redirectToRoute('step-rds');
        }

        return $this->render('document/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/step-rds", name="step-rds")
     * @Method({"GET", "POST"})
     */
    public function stepRds(Request $request, TokenGenerator $tokenGenerator, TagsCollector $tagsCollector, DelayedEventDispatcher $eventDispatcher)
    {
        $session = $request->getSession();
        $document = $this->entityManager->getRepository(Document::class)->find($session->get('document'));

        if (!$document instanceof Document) {
            return $this->redirectToRoute('homepage');
        }

        $rds = new Rds();


        $tags = $tagsCollector->TagFromPdf($document->getDocumentFile());

        if (false === $tags->hasSignTags()) {
            $this->addFlash('danger', 'Votre pdf ne contient pas de balise #SIGNXXX#');

            return $this->redirectToRoute('homepage');
        }

        $rds->setSubject($tags->getRdsSubject());
        $rds->setDescription($tags->getRdsDescription());
        $rds->setPlace($tags->getRdsPlace());
        if($tags->getRdsStartAt()) {
            $rds->setStartAt($tags->getRdsStartAt());
        }
        $form = $this->createForm(RdsType::class, $rds, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('step-rds')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rds->setDocument($document);
            $rds->setToken($tokenGenerator->generate());

            $this->entityManager->persist($rds);
            $this->entityManager->flush();

            $event = new RdsEvent($rds);
            $eventDispatcher->dispatch('app.pdf_uploaded', $event);

            return $this->redirectToRoute('step-admin');
        }

        return $this->render('rds/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/step-admin", name="step-admin")
     * @Method({"GET", "POST"})
     */
    public function stepAdmin(Request $request, TokenGenerator $tokenGenerator, TagsCollector $tagsCollector)
    {
        $session = $request->getSession();
        $rds = $this->entityManager->getRepository(Rds::class)->findOneBy(['document' => $session->get('document')]);

        if (!$rds instanceof Rds) {
            return $this->redirectToRoute('step-rds');
        }

        $admin = new Attendee();
        $tags = $tagsCollector->TagFromPdf($rds->getDocument()->getDocumentFile());

        if($tags->getAdmin()) {
            $admin->setEmail($tags->getAdmin()->getEmail());
            $admin->setFirstname($tags->getAdmin()->getFirstName());
            $admin->setLastname($tags->getAdmin()->getLastName());
            $admin->setOrganization($tags->getAdmin()->getOrganizaiton());
        }

        $form = $this->createForm(AdminType::class, $admin, [
           'method' => Request::METHOD_POST,
           'action' => $this->generateUrl('step-admin')
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $admin->setToken($tokenGenerator->generate());
            $rds->setOwner($admin);

            $this->entityManager->persist($admin);
            $this->entityManager->flush();

            return $this->redirectToRoute('step-attendee');
        }

        return $this->render('admin/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/step-attendees", name="step-attendee")
     * @Method({"GET", "POST"})
     */
    public function rdsAttendee(Request $request, DelayedEventDispatcher $eventDispatcher)
    {
        $session = $request->getSession();
        $rds = $this->entityManager->getRepository(Rds::class)->findOneBy(['document' => $session->get('document')]);

        if (!$rds instanceof Rds) {
            return $this->redirectToRoute('step-rds');
        }

        $form = $this->createForm(RdsAttendeeType::class, $rds, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('step-attendee')
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->flush();

            $session->remove('document');
            $event = new RdsEvent($rds);
            $eventDispatcher->dispatch('app.rds_complete', $event);

            return $this->redirectToRoute('rds-show', ['token' => $rds->getOwner()->getToken()]);
        }

        return $this->render('rds/attendees_form.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
