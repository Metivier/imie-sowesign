<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\GarbageCreationHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("clear-session", name="clear-session")
     */
    public function clearSession(Request $request, GarbageCreationHandler $garbageCreationHandler)
    {
        $session = $request->getSession();
        if (null !== $session) {
            $garbageCreationHandler->handle($session);
            $session->clear();
        }

        return $this->redirectToRoute('homepage');
    }
}
