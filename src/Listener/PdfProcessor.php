<?php

namespace App\Listener;

use App\Event\RdsEvent;
use App\Service\Pdf\SignedSlotCollector;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PdfProcessor implements EventSubscriberInterface
{
    /** @var SignedSlotCollector */
    private $signedSlotCollector;

    public function __construct(SignedSlotCollector $signedSlotCollector)
    {
        $this->signedSlotCollector = $signedSlotCollector;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'app.pdf_uploaded' => 'documentProcessing'
        );
    }

    public function documentProcessing(RdsEvent $event)
    {
        $document = $event->getRds()->getDocument();

        shell_exec('pdf2json '.$document->getDocumentFile()->getPathname().' '.$document->getDocumentFile()->getPathname().'.json');

        $this->signedSlotCollector->collector($event->getRds());
    }
}
