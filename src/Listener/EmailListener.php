<?php

namespace App\Listener;

use App\Event\RdsEvent;
use App\Service\Email\RdsEmailGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmailListener implements EventSubscriberInterface
{
    /** @var RdsEmailGenerator */
    private $invitaitonGenerator;
    private $mailer;

    public function __construct(RdsEmailGenerator $invitaitonGenerator, \Swift_Mailer $mailer)
    {
        $this->invitaitonGenerator = $invitaitonGenerator;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'app.rds_complete' => 'rdsComplete'
        );
    }

    public function rdsComplete(RdsEvent $event)
    {
        $rds = $event->getRds();

        foreach ($rds->getAttendees() as $attendee) {
            $message = $this->invitaitonGenerator->generate($rds, $attendee);

            $this->mailer->send($message);
        }
    }
}
