<?php

namespace App\Listener;

use App\Entity\Document;
use App\Service\Email\RdsEmailGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Filesystem\Filesystem;
use Vich\UploaderBundle\Event\Event;
use Vich\UploaderBundle\Event\Events;

class UploadPdfListener implements EventSubscriberInterface
{
    /** @var RdsEmailGenerator */
    private $invitaitonGenerator;
    private $mailer;

    public function __construct(RdsEmailGenerator $invitaitonGenerator, \Swift_Mailer $mailer)
    {
        $this->invitaitonGenerator = $invitaitonGenerator;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return array(
            Events::POST_UPLOAD => 'originalCC'
        );
    }

    public function originalCC(Event $event)
    {
        /** @var Document $document */
        $document = $event->getObject();

        if (!($document instanceof Document)){
            return;
        }

        $fs = new Filesystem();
        $documentCCPathName = $document->getDocumentFile()->getPathname().'.bak';
        $fs->copy($document->getDocumentFile()->getPathname(), $documentCCPathName);

        $document->setOriginalCopyPath($documentCCPathName);
    }
}
