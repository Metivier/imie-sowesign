<?php

namespace App\Model;

class SignatureProgress
{
    private $signature;
    private $max;

    public function __construct($signature, $max)
    {
        $this->signature = $signature;
        $this->max = $max;
    }

    public function getSignature()
    {
        return $this->signature;
    }

    public function getProgress()
    {
        return ceil($this->signature * 100 / $this->max);
    }

    public function getMax()
    {
        return $this->max;
    }
}