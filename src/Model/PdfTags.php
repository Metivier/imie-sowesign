<?php

declare(strict_types=1);

namespace App\Model;

class PdfTags
{
    /** @var null|string */
    private $rdsSubject;
    /** @var null|string */
    private $rdsDescription;
    /** @var null|string */
    private $rdsPlace;
    /** @var null|\DateTimeInterface */
    private $rdsStartAt;
    /** @var null|\DateTimeInterface */
    private $rdsEndAt;
    /** @var null|int */
    private $rdsRevivalNumber;
    /** @var null|int */
    private $rdsPeriodicity;
    /** @var null|Attendee */
    private $admin;
    /** @var boolean */
    private $hasSignTags = false;

    /**
     * @return null|string
     */
    public function getRdsSubject(): ?string
    {
        return $this->rdsSubject;
    }

    /**
     * @param null|string $rdsSubject
     */
    public function setRdsSubject(?string $rdsSubject): void
    {
        $this->rdsSubject = $rdsSubject;
    }

    /**
     * @return null|string
     */
    public function getRdsDescription(): ?string
    {
        return $this->rdsDescription;
    }

    /**
     * @param null|string $rdsDescription
     */
    public function setRdsDescription(?string $rdsDescription): void
    {
        $this->rdsDescription = $rdsDescription;
    }

    /**
     * @return null|string
     */
    public function getRdsPlace(): ?string
    {
        return $this->rdsPlace;
    }

    /**
     * @param null|string $rdsPlace
     */
    public function setRdsPlace(?string $rdsPlace): void
    {
        $this->rdsPlace = $rdsPlace;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getRdsStartAt(): ?\DateTimeInterface
    {
        return $this->rdsStartAt;
    }

    /**
     * @param \DateTimeInterface|null $rdsStartAt
     */
    public function setRdsStartAt(?\DateTimeInterface $rdsStartAt): void
    {
        $this->rdsStartAt = $rdsStartAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getRdsEndAt(): ?\DateTimeInterface
    {
        return $this->rdsEndAt;
    }

    /**
     * @param \DateTimeInterface|null $rdsEndAt
     */
    public function setRdsEndAt(?\DateTimeInterface $rdsEndAt): void
    {
        $this->rdsEndAt = $rdsEndAt;
    }

    /**
     * @return int|null
     */
    public function getRdsRevivalNumber(): ?int
    {
        return $this->rdsRevivalNumber;
    }

    /**
     * @param int|null $rdsRevivalNumber
     */
    public function setRdsRevivalNumber(?int $rdsRevivalNumber): void
    {
        $this->rdsRevivalNumber = $rdsRevivalNumber;
    }

    /**
     * @return int|null
     */
    public function getRdsPeriodicity(): ?int
    {
        return $this->rdsPeriodicity;
    }

    /**
     * @param int|null $rdsPeriodicity
     */
    public function setRdsPeriodicity(?int $rdsPeriodicity): void
    {
        $this->rdsPeriodicity = $rdsPeriodicity;
    }

    /**
     * @return Attendee|null
     */
    public function getAdmin(): ?Attendee
    {
        return $this->admin;
    }

    /**
     * @param Attendee|null $admin
     */
    public function setAdmin(?Attendee $admin): void
    {
        $this->admin = $admin;
    }

    /**
     * @return bool
     */
    public function hasSignTags(): bool
    {
        return $this->hasSignTags;
    }

    /**
     * @param bool $hasSignTags
     */
    public function setHasSignTags(bool $hasSignTags): void
    {
        $this->hasSignTags = $hasSignTags;
    }
}