<?php

declare(strict_types=1);

namespace App\Model;

class Attendee
{
    /** @var null|string */
    private $firstName;
    /** @var null|string */
    private $lastName;
    /** @var null|string */
    private $email;
    /** @var null|string */
    private $organizaiton;

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param null|string $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param null|string $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getOrganizaiton(): ?string
    {
        return $this->organizaiton;
    }

    /**
     * @param null|string $organizaiton
     */
    public function setOrganizaiton(?string $organizaiton): void
    {
        $this->organizaiton = $organizaiton;
    }
}