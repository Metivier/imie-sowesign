<?php

namespace App\Form;

use App\Entity\Rds;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RdsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject')
            ->add('description', TextareaType::class, [
                'attr' => [
                    'rows' => 5
                ]
            ])
            ->add('startAt', DateTimeType::class)
            ->add('endAt', DateTimeType::class)
            ->add('place', null, [
                'required' => false
            ])
            ->add('revivalNumber', IntegerType::class, [
                'required' => false
            ])
            ->add('periodicity', IntegerType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rds::class,
        ]);
    }
}
