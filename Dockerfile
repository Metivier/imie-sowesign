FROM gcc:latest as builder
RUN wget https://github.com/flexpaper/pdf2json/archive/v0.69.tar.gz  && tar -zxvf v0.69.tar.gz
WORKDIR ./pdf2json-0.69
RUN ./configure
RUN make
RUN make install

FROM php:7.2-fpm-stretch

RUN apt-get update && \
        apt-get --no-install-recommends --no-install-suggests --yes --quiet install \
		git \
		libicu57 \
		zlib1g

ENV APCU_VERSION 5.1.11
RUN set -xe \
	&& apt-get --no-install-recommends --no-install-suggests --yes --quiet install \
		$PHPIZE_DEPS \
		libicu-dev \
		zlib1g-dev \
		libpng-dev \
		libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
	&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-install gd \
	&& docker-php-ext-install \
		intl \
		zip \
	&& pecl install \
		apcu-${APCU_VERSION} \
    && docker-php-ext-install pdo_mysql \
	&& docker-php-ext-enable --ini-name 20-apcu.ini apcu \
	&& docker-php-ext-enable --ini-name 05-opcache.ini opcache

COPY --from=builder /usr/local/bin/pdf2json /usr/local/bin/pdf2json
COPY docker/app/php.ini /usr/local/etc/php/php.ini
COPY --from=composer:1.6 /usr/bin/composer /usr/bin/composer
COPY docker/app/docker-entrypoint.sh /usr/local/bin/docker-app-entrypoint
RUN chmod +x /usr/local/bin/docker-app-entrypoint

WORKDIR /srv/app
ENTRYPOINT ["docker-app-entrypoint"]
CMD ["php-fpm"]

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER 1

# Use prestissimo to speed up builds
RUN composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative  --no-interaction

# Allow to use development versions of Symfony
ARG STABILITY=stable
ENV STABILITY ${STABILITY}

# Allow to select skeleton version
ARG SYMFONY_VERSION=""

###> recipes ###
###< recipes ###

COPY . .

RUN mkdir -p var/cache var/logs var/sessions \
    && composer install --prefer-dist --no-scripts --no-progress --no-suggest --classmap-authoritative --no-interaction \
&& chown -R www-data var \
&& chown -R www-data public/documents