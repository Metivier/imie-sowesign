var mousePressed = false;
var touched = false;
var lastX, lastY;
var ctx;

document.addEventListener("DOMContentLoaded", function() {
    ctx = document.getElementById('canvas-sign').getContext("2d");

    $('#canvas-sign').on('mousedown', function (e) {
        e.preventDefault();
        mousePressed = true;
        touched = true;
        Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
    });

    $('#canvas-sign').on('mousemove', function (e) {
        e.preventDefault();
        if (mousePressed || touched) {
            Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
        }
    });

    $('#canvas-sign').on('mouseup touchend', function (e) {
        e.preventDefault();
        mousePressed = false;
        touched = false;
    });

    $('#canvas-sign').on('mouseleave touchleave', function (e) {
        e.preventDefault();
        mousePressed = false;
        touched = false;
    });

    $('#canvas-sign').on('touchstart', function (e) {
        e.preventDefault();
        mousePressed = true;
        touched = true;
        Draw(e.changedTouches[0].pageX - $(this).offset().left, e.changedTouches[0].pageY - $(this).offset().top, false);
    });

    $('#canvas-sign').on('touchmove', function (e) {
        e.preventDefault();
        if (touched) {
            Draw(e.changedTouches[0].pageX - $(this).offset().left, e.changedTouches[0].pageY - $(this).offset().top, true);
        }
    });

});

function Draw(x, y, isDown) {
    if (isDown) {
        ctx.beginPath();
        ctx.strokeStyle = '#00008B';
        ctx.lineWidth = $('#selWidth').val();
        ctx.lineJoin = "round";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
    }

    lastX = x;
    lastY = y;

}

function clearArea() {
    // Use the identity matrix while clearing the canvas
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function sendCanvas() {
    // Generate the image data
    var Pic = document.getElementById("canvas-sign").toDataURL("image/png");

    //Send image of signing
    $.ajax({
       type: 'POST',
       url: window.location.href,
       data: '{ "imageData" : "' + Pic + '" }',
       contentType: 'application/json; charset=utf-8',
       dataType: 'json',
       success: function () {
           window.location.reload(true);
       }
   });

}